# PROJECT INFO

TASK MANAGER (Реализованы изменение, просмотр, удаление сущностей project, task по ключам, индексам, именам.)


# DEVELOPER INFO

- NAME: Anenkov Denis

- E-mail: denk.an@inbox.ru

- Second E-mail: denk.an@bk.ru


# SOFTWARE

JDK version: 1.8

OS: MS Windows 10

# TECHNOLOGY STACK

- JAVA

- MAVEN

# MAVEN PROJECT BUILD

```bash
cd project_directory

mvn clean

mvn install
```

# PROJECT RUN

```bash
java -jar .\task-manager-java-12\target\task-manager-1.0.0.jar
```


